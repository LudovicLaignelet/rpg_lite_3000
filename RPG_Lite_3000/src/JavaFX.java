import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class JavaFX {
    Game game;

    public JavaFX(Game game) {
        this.game = game;
    }

    public void start(Stage stage) throws IOException {
        URL url = new File("C:\\Users\\ludov\\Dropbox\\8 - JAVA ISEP\\IdeaProjects\\RPG_Lite_3000\\interfaceJavaFX.fxml").toURI().toURL();
        Parent root = FXMLLoader.load(url);
        //Parent root = FXMLLoader.load(getClass().getResource("/resources/fxml/interfaceJavaFX.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("FXML Welcome");
        stage.setScene(scene);
        stage.show();

        /*
        GridPane gridPane = new GridPane();
        Button btn = CustomButtonFactory.createButton("Pick me", game.getHeroes()[0].toString());
        gridPane.add(btn, 10, 10);
        Scene scene = new Scene(gridPane, 600, 400);
        stage.setTitle("RPG_Lite_3000");
        stage.setScene(scene);
        stage.show();
         */
    }
}
